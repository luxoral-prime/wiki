# Scene

In [PrimeEngine] a `Scene` is a container for both [entities][entity] and
[systems][system]. The `Scene` handles the starting and updating of the
[systems][system]. It also contains a `name` property for identification.

## Example

A simple `Scene` with the name `Main` containing a [renderable
rectangle][entity] [`Entity`][entity] and the [systems][system]
`CanvasRenderingSystem` and [`RotatorSystem`][system]:

```ts
const scene = new Scene(
    "Main",
    [rectangle],
    [new CanvasRenderingSystem(canvas), new RotatorSystem()]
);
```

[primeengine]: index.md
[component]: component.md
[entity]: entity.md
[system]: system.md
