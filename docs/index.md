# Luxoral Prime Wiki

Welcome to the Luxoral Prime Wiki. Read the [about](about.md) if you want to
know what Luxoral Prime is. If you want to read the docs for the engine the game
is built on read [PrimeEngine About](PrimeEngine/index.md). Finally if you want
to read about the creatures in Luxoral Prime read any of the articles under the
Creatures section.
