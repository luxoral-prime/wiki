# About

Luxoral Prime is a spin off of the hit 2D PvP game Deeeep.io that takes place on
the planet of Luxoral Prime. This planet is over 90% water but is loaded with
lifeforms with never before seen biological functions to biomes that have pushed
evolution to the extreme. While Deeeep.io focuses more on PvP, Luxoral Prime
focuses on PvPvE, with a higher focus on the PvE part with a variety of features
such as 5 bosses, dangerous enviromental traps, and infections that lower your
stats. The game as of Feburary 2020 is in WIP and we expect to have V1 done by
the end of this year.
