# Carpet Chlanth

This large chlanth is the oddest of the species. Unlike most other relatives,
their legs have become huge and have a thin webbing that connects them. They
swim over the reefs searching for corals that are toxic to eat to many other
species, making them toxic to eat to most species. When outside of water, they
turn to mush due to the sacrifice of stability over mobility. When threatened,
they leave a long trail of ink that gets caught in the gills of most species,
suffocating them.

# Stats

-   Tier: 4

-   Speed: 110%

-   Health: 200.00

-   Damage: 50

-   Armor: 40%

-   Armor penetration: 0%

-   Boosts: 3

-   Oxygen: 15 seconds

-   Pressure: 50 seconds

-   Stress: 2 indiv

-   Temperature: 10 seconds

-   Poison immunity: 50%

-   Bleed reduction: 0%

-   Vision type: Electroreception

-   Immunity: 50%

-   Incognito: No

-   Weight: 4.0 Kilogram

-   Charged boost: yes

# Abilities

# Passive

-   Swimming straight for 5 seconds will increase speed by 2% (stacks by 5)
-   Immune to Toxic Corals and gains 10% more health and xp from them.

# Active

-   Using your charged boost will cause you to slowly gain 25% speed
-   When you are 25% health or lower, you can make a carpet bomb which causes
    creatures to lose oxygen when in it.
-   When hit, you deal poison damage
