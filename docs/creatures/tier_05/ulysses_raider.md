# Ulysses Raider

These dangerous creatures live on the edges of reefs, eating anything that
wanders too far from the biome. They live in groups of 2-4, competing with other
groups. Some groups will scavenge the open waters with some even living in the
drifting isles.

## Stats

-   Tier: 5

-   Speed: 100%

-   Health: 300.00

-   Damage: 60

-   Armor: 0%

-   Armor penetration: 60%

-   Boosts: 2

-   Oxygen: 15 seconds

-   Pressure: 25 seconds

-   Stress: 4 indiv

-   Temperature: 10 seconds

-   Poison immunity: 0%

-   Bleed reduction: 0%

-   Vision type: Conum

-   Immunity: 50%

-   Incognito: No

-   Weight: 80.0 Kilogram

-   Charged boost: yes

## Abilities

### Passive

-   Gains more xp when near others.
-   Mouth opens while attacking

### Active

-   When boosting, your armor penetration goes up by 20%
-   When using a charged boost, you will deal bleed damage for 5 seconds.
