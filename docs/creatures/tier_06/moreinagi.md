# Moreinagi

Living in the various tunnels and caves of the reefs, these creatures are the
leopards of the ecosystem. They are quite frail compared to Great White Raiders
and other predators, they make up with that in bite force. They can tear most
smaller prey to shreds with ease while larger creatures will come out of fights
with the creatures with nasty wounds.

# Stats

-   Tier: 6

-   Speed: 100%

-   Health: 600.00

-   Damage: 100

-   Armor: 0%

-   Armor penetration: 60%

-   Boosts: 2

-   Oxygen: 15 seconds

-   Pressure: 25 seconds

-   Stress: 4 indiv

-   Temperature: 10 seconds

-   Poison immunity: 0%

-   Bleed reduction: 0%

-   Vision type: Conum

-   Immunity: 50%

-   Incognito: No

-   Weight: 80.0 Kilogram

-   Charged boost: yes

# Abilities

# Passive

-   Turn 60% invisible in caves or near coral
-   Smell animals with 50% or less health from long distances  
    -Gain 5% more damage when near dying animals or in caves

# Active

-   Boost to gain 50% more speed and grab onto prey. If prey weights more than
    200 kg, you will grapple onto it and deal 30 every 2 seconds for 10 seconds.
    Prey can still move.
-   If prey is under 200 kg, grab onto it and shake it, dealing 20 damage per
    shake. Cannot move forward during shake, but you can move backwards. You
    hold on for 3 seconds. Can shake a max of 8 times before you deal no damage.
