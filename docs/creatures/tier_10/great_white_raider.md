# Great White Raider

**_Will be Present in V1.2 of the game_**

A much larger relative to the Ulysses Raider, this large creature lives in the
twilight zone of the ocean. They hunt much like earth’s carcharodon carcharias,
ambushing from below and crushing animals in their beaks. They are very solitary
animals, rarely interacting with other raiders. The purple markings on the
raider glow in darker parts of the ocean. This lures smaller creatures towards
the beaked maw of this creature.

# Stats

-   Tier: 10

-   Speed: 100%

-   Health: 700.00

-   Damage: 120

-   Armor: 0%

-   Armor penetration: 50%

-   Boosts: 2

-   Oxygen: 20 seconds

-   Pressure: 120 seconds

-   Stress: 1 indiv

-   Temperature: 10 seconds

-   Poison immunity: 0%

-   Bleed reduction: 30%

-   Vision type: Conum

-   Immunity: 20%

-   Incognito: No

-   Weight: 400.0 Kilogram

-   Charged boost: no

# Abilities

# Passive

-   Opens mouth when attacking

-   flips when changing directions

-   Turns 50% invisible when you are deeper.

# Active

-   When boosting upwards from deep water, it will slowly gain 40% speed. If it
    hits something, it will grab onto it for 2 seconds dealing 20 damage. Boosts
    normally going forward.
