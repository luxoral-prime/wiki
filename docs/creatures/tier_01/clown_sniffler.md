# Clown Sniffler

Much to many scientists surprise, this creature looks amazingly like A.
ocellaris, but filling an entirely different role. Its stripped body allows it
hide and live in a specialized coral. They spend most of their time sifting sand
in search of food.

# Stats

-   Tier: 1

-   Speed: 90%

-   Health: 100.00

-   Damage: 20

-   Armor: 30%

-   Armor penetration: 0%

-   Boosts: 1

-   Oxygen: 30 seconds

-   Pressure: 10 seconds

-   Stress: 10 indiv

-   Temperature: 10 seconds

-   Poison immunity: 50%

-   Bleed reduction: 50%

-   Vision type: Conum

-   Immunity: 20%

-   Incognito: No

-   Weight: 1.3 Kilogram

-   Charged boost: No

# Abilities

# Passive

-   Swim into Stripey Coral to become invisible

# Active

-   Detect food from far away
-   Both smell distance and attack (2%) increase in groups. Stacks to 10
