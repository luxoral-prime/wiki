# Nudiray

Despite having no bones, they have a plethora of specialized muscles in their
mouths that allow them to crush creatures with tough shells, allowing them to
eat the meat inside. Most species do not hunt them due to their toxic meat and
blood, which causes most species to cough them up after eating their meat.
However, this species reproduces via mitosis, splitting into new ones when
killed, allowing for them to live relatively peaceful lives.

# Stats

-   Tier: 3

-   Speed: 105%

-   Health: 200.00

-   Damage: 40

-   Armor: 0%

-   Armor penetration: 50%

-   Boosts: 2

-   Oxygen: 15 seconds

-   Pressure: 50 seconds

-   Stress: 5 indiv

-   Temperature: 10 seconds

-   Poison immunity: 10%

-   Bleed reduction: 0%

-   Vision type: Conum

-   Immunity: 50%

-   Incognito: No

-   Weight: 3.5 Kilogram

-   Charged boost: yes

# Abilities

# Passive

-   When killed, you release toxic meat and blood, poisoning any nearby
    creatures
-   25% chance of instantly respawning with 25% speed for 5 seconds.

# Active

-   When boosting, you will gain a 10% speed boost.
-   When hit, you deal poison damage
