# Coral Fatfish

While the Ulysses Raider rules the open ocean, The coral Fatfish lives in the
reefs. Its unique body shape allows for it to ambush prey in a very large
variety of ways. From burrowing to squeezing into tight spaces, this creature is
highly adept at killing small animals. When confronted by larger predators, they
will release a compound into the water that stuns the sensory system of the
creature rendering it stunned, allowing it to get away. If caught by predators,
they will be quickly met by a unpleasant meal as the flesh of these creatures is
toxic.

# Stats

-   Tier: 8

-   Speed: 100%

-   Health: 400.00

-   Damage: 50

-   Armor: 0%

-   Armor penetration: 20%

-   Boosts: 2

-   Oxygen: 10 seconds

-   Pressure: 10 seconds

-   Stress: 1 indiv

-   Temperature: 10 seconds

-   Poison immunity: 50%

-   Bleed reduction: 0%

-   Vision type: Conum

-   Immunity: 30%

-   Incognito: Yes

-   Weight: 200.0 Kilogram

-   Charged boost: yes

# Abilities

# Passive

-   Can burrow into the ground

-   Can hide in almost every hiding spot

# Active

-   When boosting, you turn into your Poi form, which releases a small pulse
    that stuns any tier 9-10 nearby with over 75% health and give you 30% more
    speed.

-   When killed, the killer becomes poisoned.
