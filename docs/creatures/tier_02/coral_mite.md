# Coral Mite

These ravenous mites are the clean up crew of the reef, eating all dead things
or dying things. They will even finish off corals that are sick. Coral mites
even will go after some of the larger reef creatures. While info is limited, a
type of rare algae seems to turn these creatures into a ravenous state beyond
comprehension. If this algae ever blooms, serious ecological events would be
felt.

# Stats

-   Tier: 2

-   Speed: 90%

-   Health: 150.00

-   Damage: 30

-   Armor: 40%

-   Armor penetration: 0%

-   Boosts: 1

-   Oxygen: 30 seconds

-   Pressure: 10 seconds

-   Stress: 5 indiv

-   Temperature: 10 seconds

-   Poison immunity: 50%

-   Bleed reduction: 50%

-   Vision type: Conum

-   Immunity: 50%

-   Incognito: No

-   Weight: 1.0 Kilogram

-   Charged boost: No

# Abilities

# Passive

-   Corpses and meat can be detected from far distances

# Active

-   Detect food from far away
-   When near creatures that are under 25% health, they become “ravenous”,
    getting a boost in damage and speed, but receive 30% more damage
